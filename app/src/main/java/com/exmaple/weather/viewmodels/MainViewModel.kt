package com.exmaple.weather.viewmodels

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.exmaple.weather.models.LocationEvent
import com.exmaple.weather.models.SearchByLatLon.SearchByLatLon
import com.exmaple.weather.models.SearchByText.SearchByText
import com.exmaple.weather.repository.WeatherRepository
import com.exmaple.weather.utils.AppConstants
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val repository: WeatherRepository): ViewModel() {

    private val _searchText = MutableStateFlow("")
    val searchText = _searchText.asStateFlow()

    private val _isSearching = MutableStateFlow(false)
    val isSearching = _isSearching.asStateFlow()

    val queryData:StateFlow<SearchByText>
        get() = repository.queryData

    val decodeLatLon:StateFlow<SearchByLatLon>
        get() = repository.decodeLatLon

    private val _latitude = MutableStateFlow<String>("")
    val latitude: StateFlow<String>
        get() = _latitude

    private val _longitude = MutableStateFlow<String>("")
    val longitude: StateFlow<String>
        get() = _longitude

    init {
        if(!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this)
        }
    }

    fun onSearchTextChange(text: String){
        _searchText.value = text
        viewModelScope.launch {
            repository.getLocationDetails(_searchText.value, AppConstants.APPID)
        }
    }

    @Subscribe
    fun receiveLocationEvent(locationEvent: LocationEvent){
        viewModelScope.launch {
            val lat = locationEvent.latitude?.let {
                _latitude.emit(it)
                Log.d("MainActivity_WeatherApp activity latitude:", it)
                it
            }
            val lon = locationEvent.longitude?.let {
                _longitude.emit(it)
                Log.d("MainActivity_WeatherApp activity longitude:", it)
                it
            }
            if (lat != null && lon != null) {
                repository.getCurrentLocationDetails(lat, lon, AppConstants.APPID)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        if(EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this)
        }
    }

}
