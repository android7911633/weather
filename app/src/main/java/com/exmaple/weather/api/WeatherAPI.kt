package com.exmaple.weather.api

import com.exmaple.weather.models.SearchByLatLon.SearchByLatLon
import com.exmaple.weather.models.SearchByText.SearchByText
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherAPI {

    @GET("/data/2.5/weather")
    suspend fun getWeather(@Query("q") searchText: String, @Query("APPID") appId: String): Response<SearchByText>

    @GET("/data/2.5/weather")
    suspend fun getWeather(@Query("lat") lat: String, @Query("lon") lon: String,@Query("APPID") appId: String): Response<SearchByLatLon>

}