package com.exmaple.weather.repository

import android.annotation.SuppressLint
import android.util.Log
import com.exmaple.weather.api.WeatherAPI
import com.exmaple.weather.models.SearchByLatLon.SearchByLatLon
import com.exmaple.weather.models.SearchByLatLon.SearchByLatLonMain
import com.exmaple.weather.models.SearchByLatLon.SearchByLatLonWind
import com.exmaple.weather.models.SearchByText.SearchByText
import com.exmaple.weather.models.SearchByText.SearchByTextMain
import com.exmaple.weather.models.SearchByText.SearchByTextWind
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

class WeatherRepository @Inject constructor(private val weatherAPI: WeatherAPI) {

    private val _queryData = MutableStateFlow<SearchByText>(
        SearchByText(
            SearchByTextMain(0,0.0), SearchByTextWind(0.0), "",
            emptyList()
        )
    )

    val queryData: StateFlow<SearchByText>
        get() = _queryData

    private val _decodeLatLon = MutableStateFlow<SearchByLatLon>(
        SearchByLatLon(
            SearchByLatLonMain(0,0.0), SearchByLatLonWind(0.0), "",
            emptyList()
        )
    )

    val decodeLatLon: StateFlow<SearchByLatLon>
        get() = _decodeLatLon

    @SuppressLint("LongLogTag")
    suspend fun getLocationDetails(query: String, appId: String) {
        Log.d("MainActivity_WeatherApp query:", query)
        val response = weatherAPI.getWeather(query, appId)
        if (response.isSuccessful && response.body() != null) {
            Log.d("MainActivity_WeatherApp response:", response.body()!!.toString())
            _queryData.emit(response.body()!!)
        }
    }

    suspend fun getCurrentLocationDetails(lat: String, lon: String, appId: String) {
        Log.d("MainActivity_WeatherApp lat:", lat)
        Log.d("MainActivity_WeatherApp lon:", lon)
        val response = weatherAPI.getWeather(lat, lon, appId)
        if (response.isSuccessful && response.body() != null) {
            Log.d("MainActivity_WeatherApp response:", response.body()!!.toString())
            _decodeLatLon.emit(response.body()!!)
        }
    }

}