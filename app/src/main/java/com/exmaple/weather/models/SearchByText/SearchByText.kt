package com.exmaple.weather.models.SearchByText

data class SearchByText(
    val main: SearchByTextMain,
    val wind: SearchByTextWind,
    val name: String,
    val weather: List<SearchByTextWeather>
)
//{
//    fun doesMatchSearchQuery(query: String): Boolean{
//        val matchingCombinations = listOf(
//            "$name"
//        )
//        return matchingCombinations.any {
//            it.contains(query, ignoreCase = true)
//        }
//    }
//}