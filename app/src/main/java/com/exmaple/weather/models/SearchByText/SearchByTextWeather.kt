package com.exmaple.weather.models.SearchByText

data class SearchByTextWeather(
    val description: String,
    val id: Int,
    val main: String
)