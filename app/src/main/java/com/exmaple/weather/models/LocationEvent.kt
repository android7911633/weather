package com.exmaple.weather.models

data class LocationEvent(
    val latitude: String?,
    val longitude: String?
)
