package com.exmaple.weather.models.SearchByLatLon

data class SearchByLatLon(
    val main: SearchByLatLonMain,
    val wind: SearchByLatLonWind,
    val name: String,
    val weather: List<SearchByLatLonWeather>
)
//{
//    fun doesMatchSearchQuery(query: String): Boolean{
//        val matchingCombinations = listOf(
//            "$name"
//        )
//        return matchingCombinations.any {
//            it.contains(query, ignoreCase = true)
//        }
//    }
//}