package com.exmaple.weather.models.SearchByLatLon

data class SearchByLatLonMain(
    val humidity: Int,
    val temp: Double
)