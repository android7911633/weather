package com.exmaple.weather.models

data class Wind(
    val speed: Double
)