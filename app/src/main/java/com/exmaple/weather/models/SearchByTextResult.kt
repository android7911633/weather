package com.exmaple.weather.models

data class SearchByTextResult(
    val main: Main,
    val wind: Wind,
    val name: String,
    val weather: List<Weather>
){
    fun doesMatchSearchQuery(query: String): Boolean{
        val matchingCombinations = listOf(
            "$name"
        )
        return matchingCombinations.any {
            it.contains(query, ignoreCase = true)
        }
    }
}