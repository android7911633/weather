package com.exmaple.weather.models.SearchByText

data class SearchByTextMain(
    val humidity: Int,
    val temp: Double
)