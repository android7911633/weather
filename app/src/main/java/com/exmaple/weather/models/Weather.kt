package com.exmaple.weather.models

data class Weather(
    val description: String,
    val id: Int,
    val main: String
)