package com.exmaple.weather.models.SearchByLatLon

data class SearchByLatLonWind(
    val speed: Double
)