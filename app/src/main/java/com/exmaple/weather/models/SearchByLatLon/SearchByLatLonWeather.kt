package com.exmaple.weather.models.SearchByLatLon

data class SearchByLatLonWeather(
    val description: String,
    val id: Int,
    val main: String
)