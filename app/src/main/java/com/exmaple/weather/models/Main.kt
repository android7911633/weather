package com.exmaple.weather.models

data class Main(
    val humidity: Int,
    val temp: Double
)